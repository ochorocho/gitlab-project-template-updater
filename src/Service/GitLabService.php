<?php

namespace Ochorocho\GitlabTemplateUpdate\Service;

use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class GitLabService
{
    public const GIT_BIN = 'git';
    private string $gitlabPath;
    private ?string $bin;

    public function __construct(string $gitlabPath)
    {
        $this->gitlabPath = $gitlabPath;
        $this->bin = (new ExecutableFinder())->find(self::GIT_BIN);
    }

    public function stash(): int
    {
        return $this->runGitCommand(['stash']);
    }

    public function push(string $branch, string $remote = 'origin'): int
    {
        return $this->runGitCommand(['push','--set-upstream', $remote, $branch]);
    }

    public function addTemplate(): int
    {
        return $this->runGitCommand(['add','-f', 'vendor/project_templates/typo3_distribution.tar.gz']);
    }

    public function commit(): int
    {
        return $this->runGitCommand(['commit','-m', 'Update TYPO3 Project Template']);
    }

    public function checkout(string $branch) {
        if($this->branchExists($branch)) {
            $this->runGitCommand(['checkout', $branch]);
        } else {
            $this->runGitCommand(['checkout', '-b', $branch]);
        }
    }

    private function branchExists(string $branch = 'task/update-typo3-project-template'): int
    {
        $process = new Process([$this->bin, 'branch', '-l', $branch], $this->gitlabPath);
        $process->start();

        while ($process->isRunning()) {
            // waiting for process to finish
        }

        return empty($process->getOutput()) ? 0 : 1;
    }

    public function exportTemplate(string $template = 'typo3_distribution'): int
    {
        $command = [
            'bundle',
            'exec',
            'rake',
            'gitlab:update_project_templates[' . $template . ']',
        ];

        return $this->run($command);
    }

    private function runGitCommand(array $command): int
    {
        $finalCommand = array_merge([ $this->bin ], $command);
        return $this->run($finalCommand);
    }

    private function run(array $command): int
    {
        $process = new Process($command, $this->gitlabPath);
        $process->setTty(1);
        $process->setTimeout(null);
        $process->setIdleTimeout(3600);

        return $process->run();
    }
}

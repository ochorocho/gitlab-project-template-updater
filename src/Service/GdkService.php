<?php

namespace Ochorocho\GitlabTemplateUpdate\Service;

use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class GdkService
{
    // @todo: Make constants configurable
    public const GDK_BIN = 'gdk';
    private string $gdkPath;
    private ?string $bin;

    public function __construct(string $gdkPath)
    {
        $this->gdkPath = $gdkPath;
        $this->bin = (new ExecutableFinder())->find(self::GDK_BIN);
    }

    public function start(): int
    {
        return $this->runGdkCommand(['start']);
    }

    public function stop(): int
    {
        return $this->runGdkCommand(['stop']);
    }

    public function update(): int
    {
        return $this->runGdkCommand(['update']);
    }

    public function run(array $command): int
    {
        $process = new Process($command, $this->gdkPath);
        $process->setTty(1);
        $process->setTimeout(null);
        $process->setIdleTimeout(3600);

        return $process->run();
    }

    private function runGdkCommand(array $command): int
    {
        $finalCommand = array_merge([ $this->bin ], $command);

        return $this->run($finalCommand);
    }
}

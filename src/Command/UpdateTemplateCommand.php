<?php

namespace Ochorocho\GitlabTemplateUpdate\Command;

use Ochorocho\GitlabTemplateUpdate\Service\GdkService;
use Ochorocho\GitlabTemplateUpdate\Service\GitLabService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'update')]
class UpdateTemplateCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('branch', InputArgument::REQUIRED, 'Target branch name?');
        $this->addArgument('gdk_path', InputArgument::OPTIONAL, 'Where is the GDK located?', '../gitlab-development-kit/');
        $this->addOption('push-branch', 'p', InputOption::VALUE_NONE, 'Push branch to GitLab');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start building the thing!');
        $gdkPath = realpath($input->getArgument('gdk_path'));
        $branch = $input->getArgument('branch');

        if (!file_exists($gdkPath)) {
            $output->writeln('<bg=yellow>Target folder ' . $gdkPath . ' does not exist!</>');
            return Command::INVALID;
        }

        $gitlabPath = $gdkPath . '/gitlab/';
        if (!file_exists($gitlabPath)) {
            $output->writeln('<bg=yellow>Target folder ' . $gdkPath . ' does not exist!</>');
            return Command::INVALID;
        }

        $gdk = new GdkService($gdkPath);
        $gitlab = new GitLabService($gitlabPath);

        // Prepare GDK so it is up-to-date
        $gdk->stop();
        // Prevent redis from crashing, dunno why this happens sometimes
        $gdk->run(['rm', 'redis/dump.rdb']);
        sleep(5);
        $output->writeln('<bg=yellow>Start building the thing!</>');
        $gitlab->stash();
        $output->writeln('<bg=yellow>Update GDK Instance!</>');
        $gdk->update();
        $gitlab->checkout('master');

        // Update postgresql - this is just one more thing
        // to take away the pain when updating the GDK
        $output->writeln('<bg=yellow>Update postgresql if needed!</>');
        $gdk->run(['support/upgrade-postgresql']);

        $output->writeln('<bg=yellow>Run me mate!</>');
        $gdk->start();
        $output->writeln('<bg=yellow>Waiting GitLab to get ready!</>');
        sleep(60);

        $gitlab->checkout($branch);
        $gitlab->exportTemplate();
        $gitlab->addTemplate();
        $gitlab->commit();
        
        if($input->getOption('push-branch')) {
            $gitlab->push($branch);
        }

        return Command::SUCCESS;
    }
}

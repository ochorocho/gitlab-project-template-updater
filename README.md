# Gitlab Project Template updater

This will update the local GDK instance and generate an archive
for the given template. It will create a now branch and
add a commit containing the archive. Finally, the branch will be
pushed to the repository.

It is recommended to use [GitLabs community fork](https://gitlab.com/gitlab-community/gitlab/) for your contributions!

⚠️This requires a GDK and your SSH key added to the root account ⚠️
⚠️ Only tested with the [TYPO3 Project Template](gitlab.com/gitlab-org/project-templates/typo3-distribution/)

## Run it

```bash
./bin/gitlab-template-update "task/update-typo3-template-october-2023" ../gitlab-development-kit
```

### Arguments

```
  branch                Target branch name?
  gdk_path              Where is the GDK located? [default: "../gitlab-development-kit/"]
```

### Options

```
  --push-branch         Push branch to GitLab
```